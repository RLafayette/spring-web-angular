import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TeacherService } from '../service/teacher.service';
import { Teacher } from '../model/Teacher';

@Component({
  selector: 'app-module',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {

  id: number;
  errors = [];
  name : string;
  email: string;
  teacher : Teacher;

  constructor(private route: ActivatedRoute, private router: Router,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.id = this.route.snapshot.params['id'];
    this.name = this.route.snapshot.params['name'];
    this.email = this.route.snapshot.params['email'];
    this.teacher = new Teacher(this.id, this.name, this.email);
    if(this.id != -1) {
      this.teacherService.retrieveTeacher(this.id).subscribe((teacher) => {
        this.teacher = teacher;
      });
    }
  }

  saveTeacher() {
    if(this.id != -1) {
      this.teacherService.createTeacher(this.teacher).subscribe(() => {
        this.router.navigate(['teachers']);
      })
    } else {
      this.teacherService.updateTeacher(this.id, this.teacher).subscribe(() => {
        this.router.navigate(['teachers']);
      })
    }
  }
}
