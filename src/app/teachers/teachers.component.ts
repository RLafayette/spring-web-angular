import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../service/authentication.service';
import { TeacherService } from '../service/teacher.service';

@Component({
  selector: 'app-modules',
  templateUrl: './teachers.component.html',
  styleUrls: ['./teachers.component.css']
})
export class TeachersComponent implements OnInit {
  teachers = [];
  name : string;
  email : string;

  constructor(private route: ActivatedRoute,
    private router: Router, 
    private authenticationService: AuthenticationService,
    private teacherService: TeacherService) { }

  ngOnInit() {
    this.retrieveTeachers();
  }

  retrieveTeachers() {
    this.teacherService.retrieveAllTeachers().subscribe(teachers => {
      this.teachers = teachers;
    });
  }

  addTeacher() {
    this.router.navigate([`/teachers/-1`]);
  }

  updateTeacher(id) {
    this.router.navigate([`/teachers/${id}`]);
  }

  deleteTeacher(id) {
    this.teacherService.deleteTeacher(id).subscribe(() => {
      this.retrieveTeachers();
    });
  }

}
