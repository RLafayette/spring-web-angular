import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Teacher } from '../model/Teacher';


@Injectable({
  providedIn: 'root'
})
export class TeacherService {

  constructor(private http: HttpClient) {

  }

  // eslint-disable-next-line
  retrieveAllTeachers() {
    //console.log('executed service')
    return this.http.get<[]>(`http://localhost:8080/api/teachers`);
  }

  // eslint-disable-next-line
  retrieveTeacher(id) {
    //console.log('executed service')
    return this.http.get<Teacher>(`http://localhost:8080/api/teachers/${id}`);
  }

  // eslint-disable-next-line
  deleteTeacher(id) {
    //console.log('executed service')
    return this.http.delete<Object>(`http://localhost:8080/api/teachers/${id}`);
  }

  // eslint-disable-next-line
  updateTeacher(id, teacher) {
    //console.log('executed service')
    return this.http.put<Object>(`http://localhost:8080/api/teachers/${id}`, teacher);
  }

  // eslint-disable-next-line
  createTeacher(teacher) {
    //console.log('executed service')
    return this.http.post<Object>(`http://localhost:8080/api/teachers/`, teacher);
  }

}
