import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TeacherService } from './teacher.service';

describe('ServiceComponent', () => {
  let component: TeacherService;
  let fixture: ComponentFixture<TeacherService>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TeacherService ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TeacherService);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
