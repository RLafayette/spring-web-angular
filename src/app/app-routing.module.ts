import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeachersComponent } from './teachers/teachers.component';
import { TeacherComponent } from './teacher/teacher.component';
import { LoginComponent } from './login/login.component';
import { LogoutComponent } from './logout/logout.component'; 
import { AuthGuardService} from './service/auth-guard.service';
const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'login', component: LoginComponent},
  {path: 'teachers', component: TeachersComponent, canActivate: [AuthGuardService]},
  {path: 'teachers/:id', component: TeacherComponent, canActivate: [AuthGuardService]},
  {path: 'logout', component: LogoutComponent,},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
